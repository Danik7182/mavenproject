package org.mik.first.services;

import org.mik.first.domain.Client;

public interface Service<T extends Client> { // type of client and child of client


    public void pay(T client);
    public void receiveService(T client);

}
