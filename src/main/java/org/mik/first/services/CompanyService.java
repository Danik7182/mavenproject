package org.mik.first.services;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.first.domain.Company;

public class CompanyService implements Service<Company> {



    private final static Boolean DEBUG_TEMPORARY =false;
    private final static Logger LOG = LogManager.getLogger();

    @Override
    public void pay(Company client) {

        if(DEBUG_TEMPORARY)
        LOG.debug("Enter pay");

    }


    @Override
    public void receiveService(Company client) {

        LOG.info("Enter receiving Service");
    }
}
