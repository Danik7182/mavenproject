package org.mik.first.domain;

import java.util.Objects;

public class Company extends Client {

    private String taxNumber;


    public Company(){

    }
    public Company(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public Company(String name, Long id, String address, String taxNumber) {
        super(name, id, address);
        this.taxNumber = taxNumber;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Company)) return false;
        if (!super.equals(o)) return false;
        Company company = (Company) o;
        return Objects.equals(taxNumber, company.taxNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), taxNumber);
    }

    @Override
    public String toString() {
        return "Company{" +
                "taxNumber='" + taxNumber + '\'' + super.toString() +
                '}';
    }
}
