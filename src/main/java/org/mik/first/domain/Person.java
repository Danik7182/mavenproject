package org.mik.first.domain;

import java.util.Objects;

public class Person extends Client{

    private String idNumber;

    public Person(){
        super();
    }

    public Person(String name, Long id, String address, String idNumber) {
        super(name, id, address);
        this.idNumber = idNumber;
    }


    public String getIdNumber(){
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        if (!super.equals(o)) return false;
        Person person = (Person) o;
        return Objects.equals(idNumber, person.idNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), idNumber);
    }


    @Override
    public String toString() {
        return "Person name is > " + getName() + ";  Address > "+ getAddress() + ";  Id >  " + getIdNumber() ;
    }
}
