package org.mik.first.domain;

import java.util.Objects;

public abstract class Client {

    private String name;
    private Long id; // primary key for DB
    private String address;

    public Client(){

    }
    public Client(String name, Long id, String address) {
        this.name = name;
        this.id = id;
        this.address = address;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name!=null || !name.isEmpty() || !name.isBlank()){

            this.name = name;
        }else {
            return;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        if(id > 0) {
            this.id = id;
        }
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return name.equals(client.name) &&
                Objects.equals(id, client.id) &&
                address.equals(client.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, address);
    }

    @Override
    public String toString() {
        return " Client{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", address='" + address + '\'' +
                '}';
    }
}
